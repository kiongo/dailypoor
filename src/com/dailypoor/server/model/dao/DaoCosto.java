package com.dailypoor.server.model.dao;

import java.util.Collection;

import javax.jdo.PersistenceManager;

import com.dailypoor.server.model.bean.Costo;
import com.dailypoor.shared.BeanParametro;
import com.dailypoor.shared.UnknownException;

public class DaoCosto {
private PersistenceManager pm;
	
	public DaoCosto(PersistenceManager pm){
		this.pm = pm;
	}
	
	public boolean mantenimiento(BeanParametro parametro) throws UnknownException{
		Querys query = new Querys(this.pm);
		return query.mantenimiento(parametro);
	}
	
	public Object getBean(Long id) throws UnknownException{
		Querys query = new Querys(this.pm);
		return query.getBean(Costo.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Costo> getListarBean() throws UnknownException{
		Querys query = new Querys(this.pm);
		Collection<Costo> lista = (Collection<Costo>) query.getListaBean(Costo.class);
		return lista;
	}
}
