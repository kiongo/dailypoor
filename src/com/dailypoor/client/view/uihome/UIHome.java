package com.dailypoor.client.view.uihome;

import com.dailypoor.client.view.uilogin.UILoginImpl;
import com.dailypoor.client.view.uimodel.UIMaskHome;
import com.dailypoor.client.view.uiregister.UIRegisterImpl;
import com.dailypoor.client.view.uisesion.UISesion;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.googlecode.mgwt.dom.client.event.touch.TouchEndEvent;
import com.googlecode.mgwt.dom.client.event.touch.TouchEndHandler;

public class UIHome extends Composite{
	private UIMaskHome uiMaskHome;	
	private DeckPanel pnlViews;
	private UIRegisterImpl uiRegister;
	private UILoginImpl uiLogin;
	private UISesion uiSesion;
	
	public UIHome(){
		initComponents();
		style();		
	}
	
	private void initComponents(){
		uiMaskHome=new UIMaskHome();
		pnlViews=new DeckPanel();
		pnlViews.setAnimationEnabled(true);
		uiRegister=new UIRegisterImpl(pnlViews);
		uiLogin=new UILoginImpl(pnlViews);
		uiSesion=new UISesion();
		pnlViews.add(uiRegister);
		pnlViews.add(uiLogin);
		pnlViews.add(uiSesion);
		pnlViews.showWidget(1);
		uiMaskHome.getPnlScrlPanel().setWidget(pnlViews);
		uiMaskHome.getPnlHeader().setCenter("DailyPoor");
		initWidget(uiMaskHome);
	}
	
	private void style(){
		
	}
	
}
