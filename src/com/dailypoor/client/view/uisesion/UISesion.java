package com.dailypoor.client.view.uisesion;

import com.dailypoor.client.resource.MyResource;
import com.dailypoor.client.view.uicosto.UICosto;
import com.dailypoor.client.view.uimodel.UICostoTabBar;
import com.dailypoor.client.view.uimodel.UIMaskHome;
import com.dailypoor.client.view.uimodel.UIProyectoTabBar;
import com.dailypoor.client.view.uiproject.UIProject;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.googlecode.mgwt.ui.client.widget.tabbar.TabPanel;

public class UISesion extends Composite{
	private UIMaskHome uiMaskHome;
	private FlowPanel pnlContenedor;
	private TabPanel tabPanel;
	private UIProyectoTabBar tabBarProyecto;
	private UICostoTabBar tabBarCosto;
	private UIProject uiProyecto;
	private UICosto uiCosto;
	public UISesion(){
		initComponents();		
		style();
	}
	
	private void initComponents(){
		uiProyecto=new UIProject();
		uiCosto=new UICosto();
		tabBarProyecto=new UIProyectoTabBar();
		tabBarCosto=new UICostoTabBar();		
		uiMaskHome=new UIMaskHome();
		uiMaskHome.getPnlHeader().setCenter("DailyPoor");
		pnlContenedor=new FlowPanel();
		tabPanel=new TabPanel();
		tabPanel.add(tabBarProyecto, uiProyecto);
		tabPanel.add(tabBarCosto, uiCosto);
		tabPanel.setDisplayTabBarOnTop(true);
		pnlContenedor.add(tabPanel);	
		uiMaskHome.getPnlScrlPanel().setWidget(pnlContenedor);		
		initWidget(uiMaskHome);
	}
	
	private void style(){
		MyResource.INSTANCE.getStlUISesion().ensureInjected();		
		tabPanel.addStyleName(MyResource.INSTANCE.getStlUISesion().tabBar());
		tabBarProyecto.addStyleName(MyResource.INSTANCE.getStlUISesion().buttonBar());
		tabBarCosto.addStyleName(MyResource.INSTANCE.getStlUISesion().buttonBar());
	}
}
