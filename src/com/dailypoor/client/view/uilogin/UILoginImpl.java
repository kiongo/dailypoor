package com.dailypoor.client.view.uilogin;

import com.dailypoor.client.service.ServiceGestionUsuario;
import com.dailypoor.client.service.ServiceGestionUsuarioAsync;
import com.dailypoor.server.model.bean.Usuario;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DeckPanel;

public class UILoginImpl extends UILogin{
	private final ServiceGestionUsuarioAsync SERVICE=GWT.create(ServiceGestionUsuario.class);
	
	public UILoginImpl(DeckPanel pnl) {
		super(pnl);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void login() {
		// TODO Auto-generated method stub
		SERVICE.logearUsuario(this.txtCorreo.getText(), this.txtClave.getText(), new AsyncCallback<Usuario>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				Window.alert(caught.getMessage());
			}

			@Override
			public void onSuccess(Usuario result) {
				// TODO Auto-generated method stub
				Window.alert(result.toString()+result.getNombre()+result.getApellidos()+result.getIdSesion());
			}});
	}
	
}
